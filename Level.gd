extends Node2D

var pineapple = preload("res://Pineapple.tscn")
var pepper = preload("res://Pepper.tscn")
var ice_cream = preload("res://IceCream.tscn")
var special_fruits = [pepper, ice_cream]
var rng = RandomNumberGenerator.new()

func _ready():
	GameState.reset()
	$HUD.hide_score()

func start_game():
	$Player.start_game()
	place_random_fruit()
	$HUD.show_score()

func _on_fruit_touched(body, fruit_touched):
	if body != $Player/Head:
		place_fruit_randomly(fruit_touched)
		return
	fruit_touched.eat()
	$HUD.update_score()
	fruit_touched.queue_free()
	place_random_fruit()

func place_random_fruit():
	var fruit = generate_fruit()
	add_child(fruit)
	place_fruit_randomly(fruit)
	fruit.connect("body_entered", _on_fruit_touched.bind(fruit))
	fruit.connect("area_entered", _on_fruit_touched.bind(fruit))

func generate_fruit():
	var pick_special_fruit = rng.randf() < 0.3
	var fruit
	if pick_special_fruit:
		if GameState.movement_speed >= 8:
			fruit = ice_cream.instantiate()
		elif GameState.movement_speed <= 2:
			fruit = pepper.instantiate()
		else:
			fruit = special_fruits[rng.randi() % special_fruits.size()].instantiate()
	else:
		fruit = pineapple.instantiate()
	fruit.set_name("Fruit")
	return fruit

func place_fruit_randomly(fruit):
	var screen_size = get_viewport().size
	fruit.position = Vector2(randi() % (50 + int(screen_size[0]) - 114), randi() % (50 + int(screen_size[1]) - 114))


func _on_game_over_restart_game():
	get_tree().reload_current_scene()


func _on_player_player_lost():
	$HUD.hide_score()
	$GameOver.game_over()
	


func _on_menu_show_how_to_play():
	$HowToPlay.show_how_to()


func _on_how_to_play_back_from_how_to_play():
	$Menu.show_menu()
