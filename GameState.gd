extends Node


var rotation_speed = 2.0
var movement_speed = 4
var segments = 5
var score = 0

func reset():
	rotation_speed = 2.0
	movement_speed = 4
	segments = 5
	score = 0
