extends CanvasLayer

signal game_start
signal show_how_to_play

func _ready():
	show_menu()

func show_menu():
	$CenterContainer/VBoxContainer/StartButton.disabled = false
	$CenterContainer/VBoxContainer/HowToPlayButton.disabled = false
	$AnimationPlayer.play("show_menu")

func hide_menu():
	$CenterContainer/VBoxContainer/StartButton.disabled = true
	$CenterContainer/VBoxContainer/HowToPlayButton.disabled = true
	$AnimationPlayer.play_backwards("show_menu")

func _on_start_button_pressed():
	hide_menu()
	emit_signal("game_start")


func _on_how_to_play_button_pressed():
	hide_menu()
	emit_signal("show_how_to_play")
