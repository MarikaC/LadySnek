extends CanvasLayer

func show_score():
	$VBoxContainer/Score.show()
	
func hide_score():
	$VBoxContainer/Score.hide()

func update_score():
	$VBoxContainer/Score.text = "Score: %d" % GameState.score
