extends Node2D

signal player_lost

var game_on = false

var previous_position = Vector2()

const segment_length = 50
const deadly_layers = 0x0A

var segment = preload("res://Segment.tscn")

func start_game():
	game_on = true

func eat_fruit(fruit):
	fruit.on_eaten()
	emit_signal("fruit_eaten", fruit)

func game_over():
	game_on = false
	$Head/Sprite2D.texture = load("res://assets/lady_snek_head_dead.png")
	print("You dead")
	emit_signal("player_lost")

func rotate_head(delta, current_direction):
	var target_direction = get_global_mouse_position() - ($Head.position + self.position)
	var rotation_angle = current_direction.angle_to(target_direction)
	var rotation_dir
	if abs(rotation_angle) <= 0.01:
		rotation_dir = 0
	elif abs(rotation_angle) <= 0.1:
		rotation_dir = rotation_angle
	elif rotation_angle > 0:
		rotation_dir = 1
	elif rotation_angle < 0:
		rotation_dir = -1
	var rotation_ammount = GameState.rotation_speed * delta
	if abs(rotation_ammount - abs(rotation_angle)) < rotation_ammount:
		$Head.rotation += rotation_angle
	else:
		$Head.rotation += rotation_dir * rotation_ammount

func move_head(current_direction):
	var current_speed = current_direction * GameState.movement_speed
	var collision = $Head.move_and_collide(current_speed)
	if collision:
		var collider = collision.get_collider()
		if collider.collision_layer & deadly_layers:
			game_over()

func add_new_segments_if_needed():
	var last_segment = $Segments.get_child($Segments.get_child_count() - 1)
	var length = segment_length * GameState.segments
	for i in length - $Segments.get_child_count():
		var segment_instance = segment.instantiate()
		segment_instance.position = last_segment.position
		segment_instance.rotation = last_segment.rotation
		$Segments.add_child(segment_instance)

func move_segments(previous_head_position, current_direction, delta):
	var first_segment = $Segments.get_child(0)
	var last_segment = $Segments.get_child($Segments.get_child_count() - 1)
	var children_created = 0
	var remaining_distance = (first_segment.position - $Head.position).length()
	if (remaining_distance > 33):
		last_segment = $Segments.get_child($Segments.get_child_count() - 1)
		last_segment.position = previous_head_position - current_direction * delta * 30 * 60
		last_segment.rotation = $Head.rotation
		$Segments.move_child(last_segment, 0)
		first_segment = last_segment
		children_created += 1
		remaining_distance = (first_segment.position - $Head.position).length()
	while (remaining_distance > 33):
		last_segment = $Segments.get_child($Segments.get_child_count() - 1)
		last_segment.position = first_segment.position + current_direction * delta * 60 * 1
		last_segment.rotation = $Head.rotation
		$Segments.move_child(last_segment, 0)
		first_segment = last_segment
		children_created += 1
		remaining_distance = (first_segment.position - $Head.position).length()

func move_tail():
	var last_segment = $Segments.get_child($Segments.get_child_count() - 1)
	$Tail.position = last_segment.position
	$Tail.position += Vector2(0, 1).rotated(last_segment.rotation) * 30
	$Tail.rotation = last_segment.rotation

func _physics_process(delta):
	if game_on:
		var previous_head_position = $Head.position
		var current_direction = Vector2(0, -1).rotated(($Head.rotation))
		
		rotate_head(delta, current_direction)
		move_head(current_direction)
		add_new_segments_if_needed()
		move_segments(previous_head_position, current_direction, delta)
		move_tail()

#create HUD
#create menu
#RELEASE


func body_out_of_bounds(body):
	if body == $Head:
		game_over()
