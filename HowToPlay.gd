extends CanvasLayer

signal back_from_how_to_play

func show_how_to():
	$CenterContainer/ColorRect/VBoxContainer/Button.disabled = false
	$AnimationPlayer.play("show_how_to")

func _on_button_pressed():
	$AnimationPlayer.play_backwards("show_how_to")
	emit_signal("back_from_how_to_play")
	$CenterContainer/ColorRect/VBoxContainer/Button.disabled = true
