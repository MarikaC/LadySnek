extends CanvasLayer

signal restart_game

func _ready():
	offset.y = 1100

func game_over():
	$CenterContainer/VBoxContainer/Score.text = "Score: %d" % GameState.score
	$AnimationPlayer.play("game_over")


func _on_replay_button_pressed():
	emit_signal("restart_game")
